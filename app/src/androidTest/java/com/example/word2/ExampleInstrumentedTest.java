package com.example.word2;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.example.word2.dao.WordDao;
import com.example.word2.db.AppDatabase;
import com.example.word2.model.Word;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Test
    public void useAppContext() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        AppDatabase db=AppDatabase.getINSTANCE(appContext);

        //assertEquals("com.example.word2", appContext.getPackageName());

        WordDao dao = db.getWordDao();
        Word word = new Word("hola");
        dao.insertWord(word);
        LiveData<List<Word>> words= dao.getWords();

        System.out.println("words:"+words);
        Log.i("words",words.toString());
       // assertEquals(word.getText(),words.get(words.size()-1).getText());
    }
}
