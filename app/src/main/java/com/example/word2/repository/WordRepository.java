package com.example.word2.repository;

import android.app.Application;
import android.app.ListActivity;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.example.word2.dao.WordDao;
import com.example.word2.db.AppDatabase;
import com.example.word2.model.Word;

import java.util.List;

public class WordRepository {

    private WordDao dao;
    private LiveData<List<Word>> words;

    public WordRepository(Application application) {
        AppDatabase db=AppDatabase.getINSTANCE(application);
        dao = db.getWordDao();
        words = dao.getWords();
    }

    public LiveData<List<Word>> getAllWords(){
        return words;
    }

    public void insert(Word word){
        new InsertAsyncTask(dao).execute(word);
    }

    private static class InsertAsyncTask extends AsyncTask<Word,Void,Void> {
        private WordDao dao;
        public InsertAsyncTask(WordDao dao) {
            this.dao=dao;
        }

        @Override
        protected Void doInBackground(Word... words) {
            dao.insertWord(words[0]);
            return null;
        }
    }

    public LiveData<Word> getWordById(int wordId) {
        return dao.getWordById(wordId);
    }
    public void updateWord(Word word) { new UpdateAsyncTask(dao).execute(word);}

    private class UpdateAsyncTask extends AsyncTask<Word,Void,Void> {
       WordDao dao;
        public UpdateAsyncTask(WordDao dao) {
            this.dao=dao;
        }

        @Override
        protected Void doInBackground(Word... words) {
            dao.updateWord(words[0]);
            return null;
        }
    }

    public void deleteWord(Word word) {new DeleteAsyncTask(dao).execute(word);}

    private class DeleteAsyncTask extends AsyncTask<Word,Void,Void> {
        WordDao dao;
        public DeleteAsyncTask(WordDao dao) {
            this.dao=dao;
        }

        @Override
        protected Void doInBackground(Word... words) {
            dao.deleteWord(words[0]);
            return null;
        }
    }
}
