package com.example.word2.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.word2.model.Word;

import java.util.List;

@Dao
public interface WordDao {

    @Insert
    void insertWord(Word word);

    @Query("SELECT * FROM words")
    LiveData<List<Word>> getWords();

    @Query("SELECT * FROM words WHERE id = :wordId")
    LiveData<Word> getWordById(int wordId);

    @Update
    void updateWord(Word word);

    @Delete
    void deleteWord(Word word);
}
