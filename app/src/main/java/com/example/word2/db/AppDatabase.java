package com.example.word2.db;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.word2.dao.WordDao;
import com.example.word2.model.Word;

@Database(entities = {Word.class}, version=1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    private static volatile AppDatabase INSTANCE;

    public static AppDatabase getINSTANCE(final Context context) {
        if (INSTANCE==null){
            synchronized (AppDatabase.class){
                if (INSTANCE==null){
                    INSTANCE= Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "words_database").build();
                }
            }
        }
        return INSTANCE;
    }

    public abstract WordDao getWordDao();
}
