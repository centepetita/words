package com.example.word2;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.word2.model.Word;
import com.example.word2.repository.WordRepository;

import java.util.List;

public class WordViewModel extends AndroidViewModel{
   private WordRepository repository;
   private LiveData<List<Word>> words;

    public WordViewModel(@NonNull Application application) {
        super(application);
        repository = new WordRepository(application);
        this.words=repository.getAllWords();
    }


    public void saveWord(Word word) {
        repository.insert(word);
    }

    public LiveData<List<Word>> getAllWords(){
        return words;
    }

    public LiveData<Word> getWord(int wordId) {
        return repository.getWordById(wordId);
    }

    public void updateWord(Word word) {
        repository.updateWord(word);
    }

    public void deleteWord(Word word) {
        repository.deleteWord(word);
    }
}
