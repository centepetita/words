package com.example.word2.ui;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.word2.R;
import com.example.word2.model.Word;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WordListAdapter extends RecyclerView.Adapter<WordListAdapter.WordViewHolder>{
    private List<Word> words;
    OnWordClickListener onWordClickListener;
    OnDeleteClickListener onDeleteClickListener;

    public WordListAdapter() {
       // this.words = new ArrayList<Word>();
    }
    public void setWords(List<Word> words) {
        this.words = words;
        notifyDataSetChanged();
    }
    public void setOnWordClickListener(OnWordClickListener onWordClickListener) {
        this.onWordClickListener = onWordClickListener;
    }
    public void setOnDeleteClickListener(OnDeleteClickListener onDeleteClickListener) {
        this.onDeleteClickListener = onDeleteClickListener;
    }



    @NonNull
    @Override
    public WordListAdapter.WordViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view=LayoutInflater.from(parent.getContext()).inflate(R.layout.word_item, parent, false);
        return new WordViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull WordListAdapter.WordViewHolder holder, int position) {
        if(words !=null) {
            Word word = words.get(position);
            holder.wordTextView.setText(word.getText());
        }
        else holder.wordTextView.setText(R.string.no_words);
    }

    @Override
    public int getItemCount() {
        if (words != null)
            return words.size();
        else return 0;
    }

    public interface OnWordClickListener {
        void onWordClicked(Word word);
    }
    public interface OnDeleteClickListener {
        void onDeleteClicked(Word word);
    }


    public class WordViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.word)
        TextView wordTextView;


        public WordViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

        @OnClick(R.id.word_row)
        public void onWordClicked(){
            Word word = words.get(getAdapterPosition());
            if(onWordClickListener!=null)
                onWordClickListener.onWordClicked(word);
        }

        @OnClick(R.id.deleteFab)
        public void onViewClicked(){
            Word word = words.get(getAdapterPosition());
            if (onDeleteClickListener!=null){
                onDeleteClickListener.onDeleteClicked(word);
            }
        }
    }
}
