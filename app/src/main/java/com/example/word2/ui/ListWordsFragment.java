package com.example.word2.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.word2.R;
import com.example.word2.WordViewModel;
import com.example.word2.model.Word;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ListWordsFragment extends Fragment {

    private WordViewModel mViewModel;
    @BindView(R.id.recyclerview)
    public RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private WordListAdapter wordAdapter;

    public static ListWordsFragment newInstance() {
        return new ListWordsFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.list_words_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(WordViewModel.class);

        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this.getActivity());
        recyclerView.setLayoutManager(layoutManager);
        wordAdapter = new WordListAdapter();
        wordAdapter.setOnWordClickListener(this::onWordClicked);
        wordAdapter.setOnDeleteClickListener(this::onDeleteClicked);
        recyclerView.setAdapter(wordAdapter);
        LiveData<List<Word>> words= mViewModel.getAllWords();


        words.observe(this, this::onWordChanged);
    }



    private void onWordChanged(List<Word> words) {
        wordAdapter.setWords(words);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @OnClick(R.id.fab)
    public void onViewClicked() {
        Navigation.findNavController(recyclerView).navigate(R.id.actionInsertWord);
        //InsertWordFragment
    }

    private void onWordClicked(Word word) {
        Integer wordId = Integer.valueOf(word.getId());
        NavDirections action = ListWordsFragmentDirections.actionViewWord(wordId);
        Navigation.findNavController(recyclerView).navigate(action);

    }

    private void onDeleteClicked(Word word){
        mViewModel.deleteWord(word);
    }
}
