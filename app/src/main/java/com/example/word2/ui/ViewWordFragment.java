package com.example.word2.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import com.example.word2.R;
import com.example.word2.WordViewModel;
import com.example.word2.model.Word;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ViewWordFragment extends Fragment {

    @BindView(R.id.wordToView)
    TextView wordToView;
    private WordViewModel mViewModel;
    private Word mWord;

    public static ViewWordFragment newInstance() {
        return new ViewWordFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.view_word_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(WordViewModel.class);
        Integer wordId= ViewWordFragmentArgs.fromBundle(getArguments()).getWordId();
        LiveData<Word>  word=mViewModel.getWord(wordId);
        word.observe(this, this::onWordChanged);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

    }

    private void onWordChanged(Word word) {
        this.mWord = word;
        wordToView.setText(word.getText());
    }

    @OnClick(R.id.fabEditWord)
    public void onViewClicked(View view) {
        NavDirections action = ViewWordFragmentDirections.actionUpdateWord(this.mWord.getId());
        Navigation.findNavController(view).navigate(action);
    }
}
