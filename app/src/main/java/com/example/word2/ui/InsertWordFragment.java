package com.example.word2.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import com.example.word2.R;
import com.example.word2.WordViewModel;
import com.example.word2.model.Word;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class InsertWordFragment extends Fragment {

    @BindView(R.id.inputWord)
    EditText inputWord;
    private WordViewModel wordViewModel;
    int wordId;

    public static InsertWordFragment newInstance() {
        return new InsertWordFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.insert_word_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        wordViewModel = ViewModelProviders.of(this).get(WordViewModel.class);
         wordId= InsertWordFragmentArgs.fromBundle(getArguments()).getWordId();
        if (wordId!=-1){
            //No arguments, navigation from wieWord: we have a word to update
            LiveData<Word> wordInserted = wordViewModel.getWord(wordId);
            wordInserted.observe(this, this::onWordChanged);

        }

    }

    private void onWordChanged(Word word) {
        inputWord.setText(word.getText());
    }

    @OnClick(R.id.btnSave)
    public void onViewClicked() {
       // Toast.makeText(getContext(), "HELLO", Toast.LENGTH_SHORT).show();
        Word word= new Word(inputWord.getText().toString());
        if (wordId==-1){
            wordViewModel.saveWord(word);
        } else{
            word.setId(wordId);
            wordViewModel.updateWord(word);
        }

        Navigation.findNavController(inputWord).popBackStack();
    }
}
